<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>GameModel</name>
    <message>
        <location filename="gamemodel.cpp" line="173"/>
        <source>Hint</source>
        <translation type="unfinished">Hinweis</translation>
    </message>
</context>
<context>
    <name>NumberSelector</name>
    <message>
        <location filename="NumberSelector.qml" line="40"/>
        <source>Clear</source>
        <translation type="unfinished">Entfernen</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="10"/>
        <location filename="main.qml" line="91"/>
        <source>SudoKunst</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="107"/>
        <source>New sudoku</source>
        <translation type="unfinished">Neues Sudoku</translation>
    </message>
    <message>
        <location filename="main.qml" line="115"/>
        <source>Beginner</source>
        <translation type="unfinished">Anfänger</translation>
    </message>
    <message>
        <location filename="main.qml" line="120"/>
        <source>Simple</source>
        <translation type="unfinished">Simpel</translation>
    </message>
    <message>
        <location filename="main.qml" line="125"/>
        <source>Easy</source>
        <translation type="unfinished">Einfach</translation>
    </message>
    <message>
        <location filename="main.qml" line="130"/>
        <source>Intermediate</source>
        <translation type="unfinished">Mittel</translation>
    </message>
    <message>
        <location filename="main.qml" line="135"/>
        <source>Expert</source>
        <translation type="unfinished">Experte</translation>
    </message>
    <message>
        <location filename="main.qml" line="144"/>
        <source>Settings</source>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="main.qml" line="153"/>
        <source>Highlight wrong numbers</source>
        <translation type="unfinished">Falsche Zahlen hervorheben</translation>
    </message>
    <message>
        <location filename="main.qml" line="159"/>
        <source>Show timer</source>
        <translation type="unfinished">Timer anzeigen</translation>
    </message>
    <message>
        <location filename="main.qml" line="165"/>
        <source>Use draw input</source>
        <translation type="unfinished">Maleingabe verwenden</translation>
    </message>
    <message>
        <location filename="main.qml" line="168"/>
        <source>Cheat</source>
        <translation type="unfinished">Mogeln</translation>
    </message>
    <message>
        <location filename="main.qml" line="197"/>
        <source>Contratulations!
You solved the puzzle!</source>
        <translation type="unfinished">Gratulation!
Du hast das Puzzle gelöst!</translation>
    </message>
</context>
</TS>
