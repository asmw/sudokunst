import QtQuick 2.0

Item {
    id: cell
    width: 100
    height: 100

    property bool readonly: !model.editable
    property var value: model.puzzleValue
    property int xC
    property int yC
    property bool showHint: false
    property bool useDrawInput: true
    property int paintThreshold: 8

    function setValue(value) {
        model.edit = value;
        drawInput.reset();
    }

    signal requestValue(var cell)

    Rectangle {
        anchors.fill: parent
        id: background
        color: "white"
        opacity: 0.6
        border.width: 1
        border.color: (showHint && model.hint) ? "red" : "#090909"
    }

    Canvas {
        id: drawInput
        anchors.fill: parent
        visible: useDrawInput && !cell.readonly && (mousearea.pressed || detectTimer.running)

        property int prevX: -1
        property int prevY: -1
        property int newX: -1
        property int newY: -1
        property int lineWidth: 5
        property int paintCount: 0

        function reset() {
            prevX = -1
            prevY = -1
            newX = -1
            newY = -1
            paintCount = 0;
            requestPaint()
        }

        function draw(x, y) {
            newX = x;
            newY = y;
            requestPaint();
        }

        function getData() {
            return toDataURL();
        }

        Timer {
            id: detectTimer
            interval: 1000
            onTriggered: {
                if (drawInput.paintCount > cell.paintThreshold) {
                    cell.setValue(classifier.classify(drawInput.getData()));
                } else {
                    drawInput.reset();
                }
            }
        }

        onPaint: {
            drawInput.paintCount++;
            var ctx = getContext('2d');
            if (prevX >= 0) {
                ctx.lineCap = "round";
                ctx.strokeStyle = "black"
                ctx.lineWidth = drawInput.lineWidth;
                ctx.beginPath();
                ctx.moveTo(prevX, prevY);
                ctx.lineTo(newX, newY);
                ctx.stroke();
                ctx.closePath();
            }
            if (newX >= 0) {
                prevX = newX;
                prevY = newY;
            } else {
                ctx.fillStyle = "white";
                ctx.fillRect(0,0,width,height);
            }
        }
    }

    Text {
        id: number
        text: cell.value
        anchors.centerIn: parent
        font.family: "sans"
        font.bold: cell.readonly
        font.pixelSize: parent.height * 0.8
    }

    MouseArea {
        id:mousearea
        enabled: !cell.readonly
        anchors.fill: drawInput
        onPressed: {
            if (!useDrawInput)
                return
            detectTimer.stop();
            drawInput.prevX = mouseX;
            drawInput.prevY = mouseY;
        }
        onReleased: {
            if (useDrawInput) {
                drawInput.prevX = -1;
                drawInput.prevY = -1;
                detectTimer.restart();
            } else {
                cell.requestValue(cell)
            }
        }
        onPositionChanged: drawInput.draw(mouseX, mouseY)
    }
}
