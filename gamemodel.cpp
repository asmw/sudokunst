#include "gamemodel.h"

#include <QFile>
#include <QRandomGenerator>

#include <QDebug>

GameModel::GameModel(QObject *parent)
    : QAbstractListModel(parent),
      _userInput(81, '.')
{
}

QModelIndex GameModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!parent.isValid())
        return createIndex(row, column);
    return QModelIndex();
}

QModelIndex GameModel::parent(const QModelIndex &index) const
{
    return QModelIndex();
}

int GameModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 9 * 9;
    return 0;
}

int GameModel::columnCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 1;
    return 0;
}

QVariant GameModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    switch(role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
    case PuzzleValue:
        return puzzleValue(index.row());
    case HintRole:
        return hint(index.row());
    case Editable:
        return _currentPuzzle.isEmpty() ? false : _currentPuzzle.at(index.row()) == '.';
    }
    return QVariant();
}

bool GameModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        const auto val = value.toString().at(0);
        if (val != '0') {
            _userInput[index.row()] = val;
        } else {
            _userInput[index.row()] = '.';
        }
        emit dataChanged(index, index, QVector<int>() << PuzzleValue << HintRole);
        if (solved()) {
            solvedChanged();
            runningChanged();
        }
        return true;
    }
    return false;
}

Qt::ItemFlags GameModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    if (puzzleValue(index.row()) != '.')
        return Qt::ItemIsEditable;
    return Qt::NoItemFlags;
}

QHash<int, QByteArray> GameModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PuzzleValue] = "puzzleValue";
    roles[HintRole] = "hint";
    roles[Editable] = "editable";
    roles[Qt::EditRole] = "edit";
    return roles;
}

void GameModel::setPuzzle(const QString &data)
{
    const auto parts = data.split(",");
    beginResetModel();
    _currentPuzzle = parts.at(0).trimmed();
    _userInput = _currentPuzzle;
    _solution = parts.at(1).trimmed();
    if (qEnvironmentVariableIsSet("SUDOKUNST_DUMP_PUZZLES")) {
        for (int y = 0; y < 9; ++y) {
            QStringList line;
            for (int x = 0; x < 9; ++x) {
                line << _solution.at(y * 9 + x);
            }
            qDebug() << line.join(" ");
        }
    }
    endResetModel();
    solvedChanged();
    runningChanged();
    puzzleLoaded();
}

bool GameModel::solved() const
{
    return _userInput == _solution;
}

bool GameModel::running() const
{
    return !_currentPuzzle.isEmpty() && !solved();
}

void GameModel::loadPuzzle(const GameModel::Difficulty difficulty, const int number)
{
    QFile puzzleFile(":/puzzles/sudoku_" + difficultyToString(difficulty) + ".txt");
    if (puzzleFile.open(QFile::ReadOnly)) {
        const auto data = puzzleFile.readAll().split('\n');
        const int puzzleCount = data.length() - 1;
        if (number < 0) {
            setPuzzle(data.at(QRandomGenerator::system()->bounded(puzzleCount)));
        } else if (number > data.count()) {
            qWarning() << "Puzzle" << number << "requested, but only" << data.count() << "available.";
        } else {
            setPuzzle(data.at(number));
        }
    }
}

void GameModel::cheat()
{
    beginResetModel();
    _userInput = _solution;
    for (int x = 0; x < _currentPuzzle.length(); ++x) {
        if (_currentPuzzle.at(x) == '.') {
            _userInput[x] = '.';
            break;
        }
    }
    endResetModel();
}

QString GameModel::puzzleValue(int idx) const
{
    if (!_currentPuzzle.isEmpty()) {
        const auto value = _userInput.at(idx);
        if (value != '.') {
            return value;
        }
    }
    return QString();
}

QString GameModel::hint(int idx) const
{
    const auto input = _userInput.at(idx);
    if (input != '.' && input != _solution.at(idx))
        return QString(tr("Hint"));
    return QString();
}
