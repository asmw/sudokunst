#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QAbstractListModel>

class GameModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool running READ running NOTIFY runningChanged)
    Q_PROPERTY(bool solved READ solved NOTIFY solvedChanged)

public:
    enum GameRoles {
        PuzzleValue = Qt::UserRole + 1,
        HintRole,
        Editable,
    };

    enum Difficulty {
        Beginner,
        Simple,
        Easy,
        Intermediate,
        Expert,
    };
    Q_ENUM(Difficulty);

    static QString difficultyToString(const Difficulty difficulty) {
        switch (difficulty) {
        case Beginner: return "beginner";
        case Simple: return "simple";
        case Easy: return "easy";
        case Intermediate: return "intermediate";
        case Expert: return "expert";
        }
        return QString();
    }

    explicit GameModel(QObject *parent = nullptr);

    // Basic functionality:
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    QHash<int, QByteArray> roleNames() const override;

    void setPuzzle(const QString &data);

    bool solved() const;
    bool running() const;

public slots:
    void loadPuzzle(const Difficulty difficulty, const int number = -1);
    void cheat();

signals:
    void solvedChanged();
    void runningChanged();
    void puzzleLoaded();

private:
    QString puzzleValue(int idx) const;
    QString hint(int idx) const;

    QString _currentPuzzle;
    QString _solution;
    QString _userInput;

    QStringList _imageData;
};

#endif // GAMEMODEL_H
