import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

ApplicationWindow {
    id: window
    visible: true
    width: 480
    height: 640
    title: qsTr("SudoKunst")

    property int availableSize: Math.min(width, height - header.height)

    function loadPuzzle(difficulty) {
        drawer.close();
        game.loadPuzzle(difficulty);
    }

    Component.onCompleted: loadPuzzle(game.Beginner)

    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                id: menuButton
                font.family: iconFontFamily
                font.bold: true

                text: drawer.opened ? "\uf053" : "\uf0c9"
                onClicked: if (drawer.position == 0) { drawer.open() }

                Component.onCompleted: {
                    console.log(iconFontFamily)
                }
            }

            Item {
                Layout.fillWidth: true
            }

            Label {
                id: timer

                property var startTime
                property var timeText: "00:00:00"

                Layout.margins: 5
                horizontalAlignment: Label.AlignHCenter
                visible: timerCheckbox.checked
                text: qsTr("Time: " + timeText)

                function update() {
                    if (!startTime)
                        return;

                    var currentTime = new Date();
                    var seconds = Math.floor((currentTime - startTime)/1000);

                    var hours = Math.floor(seconds / 60 / 60);
                    var minutes = Math.floor(seconds / 60) - (hours * 60);
                    var sec = seconds % 60;

                    timeText =  hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0') + ':' + sec.toString().padStart(2, '0');
                }

                function reset() {
                    timer.startTime = new Date();
                    timeText = "00:00:00";
                }

                Connections {
                    target: game
                    function onPuzzleLoaded() {
                        timer.reset();
                    }
                }

                Timer {
                    id: gameTimer
                    interval: 1000
                    repeat: true
                    running: game && game.running
                    onTriggered: timer.update();
                }
            }
        }
        Label {
            anchors.centerIn: parent
            font.bold: true
            font.pointSize: 14
            text: qsTr("SudoKunst")
        }
    }

    Drawer {
        id: drawer
        y: header.height
        width: window.width * 0.6
        height: window.height - header.height

        ColumnLayout {
            width: parent.width
            GroupBox {
                Layout.fillWidth: true
                Layout.margins: 5
                label: Label {
                    text: qsTr("New sudoku")
                    font.pointSize: 12
                    font.bold: true
                }
                ColumnLayout {
                    anchors.fill: parent
                    Button {
                        Layout.fillWidth: true
                        text: qsTr("Beginner")
                        onClicked: window.loadPuzzle(game.Beginner);
                    }
                    Button {
                        Layout.fillWidth: true
                        text: qsTr("Simple")
                        onClicked: window.loadPuzzle(game.Simple);
                    }
                    Button {
                        Layout.fillWidth: true
                        text: qsTr("Easy")
                        onClicked: window.loadPuzzle(game.Easy);
                    }
                    Button {
                        Layout.fillWidth: true
                        text: qsTr("Intermediate")
                        onClicked: window.loadPuzzle(game.Intermediate);
                    }
                    Button {
                        Layout.fillWidth: true
                        text: qsTr("Expert")
                        onClicked: window.loadPuzzle(game.Expert);
                    }
                }
            }
            GroupBox {
                Layout.fillWidth: true
                Layout.margins: 5
                label: Label {
                    text: qsTr("Settings")
                    font.pointSize: 12
                    font.bold: true
                }
                ColumnLayout {
                    anchors.fill: parent
                    CheckBox {
                        id: hintCheckbox
                        Layout.fillWidth: true
                        text: qsTr("Highlight wrong numbers")
                    }
                    CheckBox {
                        id: timerCheckbox
                        checked: true
                        Layout.fillWidth: true
                        text: qsTr("Show timer")
                    }
                    CheckBox {
                        id: drawCheckbox
                        checked: true
                        Layout.fillWidth: true
                        text: qsTr("Use draw input")
                    }
                    Button {
                        text: qsTr("Cheat")
                        onClicked: game.cheat()
                    }
                }
            }
        }
    }

    SudokuGrid {
        id: sudoku
        anchors.centerIn: parent
        width: window.availableSize * 0.95
        height: width
        showHints: hintCheckbox.checked
        useDrawInput: drawCheckbox.checked
        enabled: !winScreen.visible

        onRequestValue: numberSelector.cell = cell
    }

    Rectangle {
        id: winScreen
        color: "#d4ffd4"
        radius: 2
        anchors.fill: parent
        anchors.margins: 5
        visible: game && game.solved
        Label {
            anchors.centerIn: parent
            horizontalAlignment: Label.AlignHCenter
            text: qsTr("Contratulations!\nYou solved the puzzle!")
            font.pointSize: 30
        }
    }

    NumberSelector {
        id: numberSelector
        anchors.centerIn: parent
        width: window.availableSize * 0.8
        height: width
        visible: cell !== undefined

        property var cell: undefined

        onSelected: {
            cell.setValue(number);
            cell = undefined;
        }
    }
}
