#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QFile>
#include <QFontDatabase>

#include "classifier.h"
#include "gamemodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QFontDatabase fontDb;
    QString iconFontFamily;
    const int iconFontId = fontDb.addApplicationFont(":/third-party/font-awesome/fa-solid-900.ttf");
    if (iconFontId == -1) {
        qWarning() << "Failed to load FontAwesome";
    } else {
        iconFontFamily = fontDb.applicationFontFamilies(iconFontId).first();
    }

    if (!QFile::exists("tiny_dnn_mnist.model")) {
        qDebug() << "Unpacking model";
        QFile resourceModel(":/tiny_dnn_mnist.model");
        resourceModel.copy("tiny_dnn_mnist.model");
    }

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    Classifier cl;
    GameModel game;

    engine.rootContext()->setContextProperty("classifier", &cl);
    engine.rootContext()->setContextProperty("game", &game);
    engine.rootContext()->setContextProperty("iconFontFamily", iconFontFamily);
    engine.load(url);

    return app.exec();
}
