#!/bin/bash

COUNT=${1:-1000}

LEVELS="beginner simple easy intermediate expert"

for level in $LEVELS; do
    echo "Generating $COUNT $level puzzles"
    java -jar qqwing.jar --symmetry none --generate $COUNT --difficulty $level --csv --solution | tail -n +2 > sudoku_${level}.txt
done
