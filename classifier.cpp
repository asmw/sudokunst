#include "classifier.h"

inline float dataVal(const QRgb color) {
    return (float(-2 * qGray(color)) / 255.0) + 1;
}

// rescale output to 0-100
template <typename Activation>
double rescale(double x) {
    Activation a(1);
    return 100.0 * (x - a.scale().first) / (a.scale().second - a.scale().first);
}

Classifier::Classifier(QObject *parent) : QObject(parent)
{
    try {
        nn.load("tiny_dnn_mnist.model");
    } catch (...) {
        qFatal("Model could not be loaded");
    }
}

int Classifier::classify(const QByteArray &dataUrl) {
    const auto parts = dataUrl.split(',');
    if (parts.size() > 1) {
        const auto image = QImage::fromData(QByteArray::fromBase64(parts.at(1))).scaled(32, 32);

        // recognize
        const auto res = nn.predict(imageData(image));
        std::vector<std::pair<double, int>> scores;

        // sort & print top-3
        for (int i = 0; i < 10; i++)
            scores.emplace_back(rescale<tiny_dnn::tanh_layer>(res[i]), i);

        sort(scores.begin(), scores.end(), std::greater<std::pair<double, int>>());

        return scores.front().second;
    }
    return -1;
}

tiny_dnn::vec_t Classifier::imageData(const QImage &image)
{
    tiny_dnn::vec_t result;
    for(int y = 0; y < image.height(); ++y) {
        for(int x = 0; x < image.width(); ++x) {
            result.push_back(dataVal(image.pixel(x,y)));
        }
    }
    return result;
}

void Classifier::asciiDump(const QImage &image)
{
    for(int y = 0; y < image.height(); ++y) {
        QStringList line;
        for(int x = 0; x < image.width(); ++x) {
            const auto value = dataVal(image.pixel(x,y));
            if (value != -1)
                line << QString::number(value, 'g', 2).rightJustified(5);
            else
                line << "     ";
        }
        qDebug() << line.join(" ");
    }
}
