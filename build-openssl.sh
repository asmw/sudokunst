#!/bin/bash
set -u
set -e

export BUILD_ARCHS=${BUILD_ARCHS:-arm_32 arm_64}
export OPENSSL_BRANCH=OpenSSL_1_1_1-stable
export OPENSSL_ANDROID_API=21

NDK=${1:-$NDK}

export ANDROID_NDK_ROOT=$NDK

export BASE=$(realpath ${BASE:-$(pwd)})

export PREFIX=$BASE/install
mkdir -p $PREFIX

cd $BASE
if [ ! -d openssl ]; then
    git clone --depth 1 git://git.openssl.org/openssl.git --branch $OPENSSL_BRANCH
else
    echo "Remove the openssl directory to rebuild"
    exit 1
fi
cd openssl
echo "Building OpenSSL in $(realpath $PWD), deploying to $PREFIX"

export PATH=$NDK/toolchains/llvm/prebuilt/linux-x86_64/bin:$PATH

if [[ "$BUILD_ARCHS" = *"arm_32"* ]]; then
    ./Configure shared android-arm -D__ANDROID_API__=$OPENSSL_ANDROID_API --prefix=$PREFIX
    make clean
    make depend
    make -j$(nproc) build_libs SHLIB_VERSION_NUMBER= SHLIB_EXT=_armeabi-v7a.so
    make -j$(nproc) install_sw SHLIB_VERSION_NUMBER= SHLIB_EXT=_armeabi-v7a.so
fi

if [[ "$BUILD_ARCHS" = *"arm_64"* ]]; then
    ./Configure shared android-arm64 -D__ANDROID_API__=$OPENSSL_ANDROID_API --prefix=$PREFIX
    make clean
    make depend
    make -j$(nproc) build_libs SHLIB_VERSION_NUMBER= SHLIB_EXT=_arm64-v8a.so
    make -j$(nproc) install_sw SHLIB_VERSION_NUMBER= SHLIB_EXT=_arm64-v8a.so
fi

if [[ "$BUILD_ARCHS" = *"x86_32"* ]]; then
    ./Configure shared android-x86 -D__ANDROID_API__=$OPENSSL_ANDROID_API --prefix=$PREFIX
    make clean
    make depend
    make -j$(nproc) build_libs SHLIB_VERSION_NUMBER= SHLIB_EXT=_x86.so
    make -j$(nproc) install_sw SHLIB_VERSION_NUMBER= SHLIB_EXT=_x86.so
fi

if [[ "$BUILD_ARCHS" = *"x86_64"* ]]; then
    ./Configure shared android-x86_64 -D__ANDROID_API__=$OPENSSL_ANDROID_API --prefix=$PREFIX
    make clean
    make depend
    make -j$(nproc) build_libs SHLIB_VERSION_NUMBER= SHLIB_EXT=_x86_64.so
    make -j$(nproc) install_sw SHLIB_VERSION_NUMBER= SHLIB_EXT=_x86_64.so
fi
