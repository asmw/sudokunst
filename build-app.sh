#!/bin/bash
set -u
set -e

NDK=${1:-$NDK}
SDK=${2:-$SDK}

export ANDROID_ARCH=arm
export ANDROID_ARCH_ABI=armeabi-v7a
export ANDROID_NDK=$NDK
export ANDROID_NDK_ROOT=$NDK
export ANDROID_SDK_ROOT=$SDK

export BASE=$(realpath ${BASE:-$(pwd)})
export PREFIX=$BASE/install

echo "Building Sudokunst"
mkdir build
cd build
$PREFIX/bin/qmake ..
make -j$(nproc)
make -j$(nproc) apk_install_target
$PREFIX/bin/androiddeployqt --release --output android-build --verbose --input android-sudokunst-deployment-settings.json --gradle
