import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Item {
    id: numberSelector

    signal selected(var number)

    width: 100
    height: 100

    Rectangle {
        id: background
        anchors.fill: parent
        color: "#f3f3f3"
        opacity: 0.9
    }

    GridLayout {
        anchors.fill: parent
        anchors.margins: parent.width * 0.1
        rows: 3
        columns: 3

        Repeater {
            model: 9
            Rectangle {
                border.color: "darkgrey"
                border.width: 2
                Layout.fillHeight: true
                Layout.fillWidth: true
                radius: 5

                color: mousearea.pressed ? "darkgrey" : "lightgrey"

                Label {
                    anchors.centerIn: parent
                    font.pixelSize: parent.height * 0.3
                    text: index != 0 ? index : qsTr("Clear")
                }

                MouseArea {
                    id: mousearea
                    anchors.fill: parent
                    onClicked: numberSelector.selected(index)
                }
            }
        }
    }

}
