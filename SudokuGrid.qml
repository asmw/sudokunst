import QtQuick 2.0

import QtQuick.Layouts 1.12

Item {
    id: root

    property bool showHints: false
    property bool useDrawInput: true

    signal requestValue(var cell)

    GridLayout {
        id: backgrounds
        anchors.fill: parent

        rows: 3
        columns: rows

        Repeater {
            model: parent.rows * parent.columns
            delegate: SudokuGroup {
                color: index % 2 === 0 ? "#404040" : "lightgrey"
                z: index % 2 === 0 ? 2 : 1
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }

    GridLayout {
        id: fields
        anchors.fill: parent

        rows: 9
        columns: rows

        Repeater {
            model: game //fields.rows * fields.columns
            delegate: SudokuCell {
                xC: index % fields.columns
                yC: index / fields.rows
                Layout.fillWidth: true
                Layout.fillHeight: true
                showHint: root.showHints
                onRequestValue: root.requestValue(cell)
                useDrawInput: root.useDrawInput
            }
        }
    }
}
