#!/bin/bash
set -u
set -e

NDK=${1:-$NDK}
SDK=${2:-$SDK}

export QT_VERSION=5.15.0
export QT_ABIS=${QT_ABIS:-}
export BUILD_ARCHS=${BUILD_ARCHS:-arm_32}

if [ -z "$QT_ABIS" ]; then
  if [[ "$BUILD_ARCHS" = *"arm_32"* ]]; then
    QT_ABIS="armeabi-v7a"
  fi

  if [[ "$BUILD_ARCHS" = *"arm_64"* ]]; then
    QT_ABIS="$QT_ABIS arm64-v8a"
  fi

  if [[ "$BUILD_ARCHS" = *"x86_32"* ]]; then
    QT_ABIS="$QT_ABIS x86"
  fi

  if [[ "$BUILD_ARCHS" = *"x86_64"* ]]; then
    QT_ABIS="$QT_ABIS x86_64"
  fi
fi
QT_ABIS=$(echo $QT_ABIS | xargs | tr " " ",")
echo "Building Qt ABIS: <$QT_ABIS>"

export BASE=$(realpath ${BASE:-$(pwd)})

export QT=$BASE/qt
mkdir -p $QT

export PREFIX=$BASE/install
mkdir -p $PREFIX

echo "Building Qt to: $PREFIX"
cd $QT
if [ ! -d qtbase ]; then
	git clone --depth 1 https://code.qt.io/qt/qtbase.git --single-branch --branch $QT_VERSION
fi
pushd qtbase
rm -f config.cache
./configure -xplatform android-clang \
	--disable-rpath \
	-openssl-linked \
	-nomake tests \
	-nomake examples \
	-android-ndk $NDK \
	-android-sdk $SDK \
	-android-abis $QT_ABIS \
	-no-warnings-are-errors \
	-opensource \
	-confirm-license \
	-prefix $PREFIX \
	-I$PREFIX/include \
	-L$PREFIX/lib
make -j$(nproc)
make install
popd

echo "Building Qt Declarative"
if [ ! -d qtdeclarative ]; then
	git clone --depth 1 https://code.qt.io/qt/qtdeclarative.git --single-branch --branch=$QT_VERSION
fi
pushd qtdeclarative
$PREFIX/bin/qmake .
make -j$(nproc)
make install
popd

echo "Building Qt Quick Controls 2"
if [ ! -d qtquickcontrols2 ]; then
	git clone --depth 1 https://code.qt.io/qt/qtquickcontrols2.git --single-branch --branch=$QT_VERSION
fi
pushd qtquickcontrols2
$PREFIX/bin/qmake .
make -j$(nproc)
make install
popd
