#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <QObject>
#include <QJSValue>
#include <QImage>
#include <QByteArray>

#include "tiny_dnn/tiny_dnn.h"
#include "tiny_dnn/network.h"

#include <QDebug>

class Classifier : public QObject
{
    Q_OBJECT

public:
    Classifier(QObject *parent = nullptr);

public slots:
    int classify(const QByteArray &dataUrl);

private:
    static tiny_dnn::vec_t imageData(const QImage &image);
    static void asciiDump(const QImage &image);

    tiny_dnn::network<tiny_dnn::sequential> nn;
};

#endif // CLASSIFIER_H
