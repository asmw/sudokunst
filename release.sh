#!/bin/bash
set -e
set -u

PRO_FILE=sudokunst.pro
VERSION_NAME=$1
VERSION_CODE=${2:-$(($(cat $PRO_FILE | grep VERSION_CODE | awk -F= '{print $2}') + 1))}

echo Setting android version $VERSION_NAME, code $VERSION_CODE

sed -i -e "s/\(VERSION_NAME=\).*/\1${VERSION_NAME}/" $PRO_FILE
sed -i -e "s/\(VERSION_CODE=\).*/\1${VERSION_CODE}/" $PRO_FILE

git add $PRO_FILE
git commit -m "Android release: version $VERSION_NAME, code $VERSION_CODE"
git tag "$VERSION_NAME"
