import QtQuick 2.0

import QtQuick.Layouts 1.12

Rectangle {
    Layout.margins: -3
    color: root.background
    border.color: color
    border.width: 1
}
